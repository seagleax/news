<?php

namespace App\Http\Responses\Api\News;

use App\Models\News\Comment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Support\Responsable;

class NewsCommentResponse implements Responsable, ShouldQueue
{
    use Queueable;

    public function toResponse($request)
    {
        try {
            $this->create($request);

            $data['code'] = 200;
            $data['message'] = 'Ok';
            $data['data'] = new \stdClass;
        } catch (Exception $e) {
            $data['code'] = 500;
            $data['message'] = $e->getMessage();
            $data['data'] = new \stdClass;
        }

        return response()->json($data);
    }

    protected function create($request)
    {
        Comment::create([
            'news_id' => $request->news_id,
            'comment_name' => $request->name,
            'comment_email' => $request->email,
            'comment_content' => $request->comment,
            'status' => '1',
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}
