<?php

namespace App\Http\Responses\Api\News;

use App\Models\News\News;
use Illuminate\Contracts\Support\Responsable;

class NewsDetailResponse implements Responsable
{
    public function toResponse($request)
    {
        try {
            $data = $this->data($request);

            if ($data) {
                return response()->json([
                    'code' => 200,
                    'message' => 'Ok',
                    'data' => $data,
                ], 200);
            }else{
                return response()->json([
                    'code' => 204,
                    'message' => 'No Content',
                    'data' => new \stdClass,
                ], 200);
            }
        } catch (\Exception $e) {
            return response()->json([
                'code' => 500,
                'message' => $e->getMessage(),
                'data' => new \stdClass
            ], 200);
        }
    }

    protected function data($request)
    {
        return News::query()
            ->select($this->query())
            ->join('admin_master', 'admin_master.admin_id', '=', 'news_master.created_by')
            ->with(['comment'])
            ->where('admin_master.status', '1')
            ->where('news_master.status', '1')
            ->where('news_master.news_id', $request->news_id)
            ->first();
    }

    public function query()
    {
        return [
            'news_id',
            'news_title',
            'admin_name as created_by',
            'news_master.created_at',
            'news_image',
            'news_content',
        ];
    }
}
