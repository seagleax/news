<?php

namespace App\Http\Controllers\Api\News;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Responses\Api\News\NewsResponse;
use App\Http\Responses\Api\News\NewsDetailResponse;
use App\Http\Responses\Api\News\NewsCommentResponse;

class NewsController extends Controller
{
    public function index(Request $request)
    {
        return new NewsResponse;
    }

    public function detail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'news_id' => 'required',
        ]);

        if($validator->fails()) {
            return response()->json([
                'code' => 422,
                'message' => $validator->errors()->first(),
            ], 200);
        }

        return new NewsDetailResponse;
    }

    public function comment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'news_id' => 'required|exists:news_master',
            'name' => 'required',
            'email' => 'required|email',
            'comment' => 'required|max:500'
        ]);

        if($validator->fails()) {
            return response()->json([
                'code' => 422,
                'message' => $validator->errors()->first(),
            ], 200);
        }

        return new NewsCommentResponse;
    }
}
