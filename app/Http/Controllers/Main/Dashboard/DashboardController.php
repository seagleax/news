<?php

namespace App\Http\Controllers\Main\Dashboard;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        $data['title'] = 'Dashboard Analytic';
        return view('page.dashboard.view', $data);
    }
}
