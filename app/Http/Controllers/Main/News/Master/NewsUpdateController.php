<?php

namespace App\Http\Controllers\Main\News\Master;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Responses\Web\News\Master\NewsEditResponse;
use App\Http\Responses\Web\News\Master\NewsUpdateResponse;

class NewsUpdateController extends Controller
{
    public function edit(Request $request)
    {
        $auth = $this->authorize('view');
        if (!$auth) {
            return $this->redirect();
        }

        return new NewsEditResponse;
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'news_title' => [
                'required',
                Rule::unique('news_master', 'news_title')->where(function ($query) use($request){
                    return $query->where('news_id', '!=', $request->news_id);
                })
            ],
            'news_content' => 'required',
            'news_image' => 'required',
        ]);

        if($validator->fails()) {
            return response()->json([
                'code' => 422,
                'message' => $validator->errors()->first(),
            ], 200);
        }

        return new NewsUpdateResponse;
    }
}
