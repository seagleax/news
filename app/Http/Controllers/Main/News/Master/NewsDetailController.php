<?php

namespace App\Http\Controllers\Main\News\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Responses\Web\News\Master\NewsDetailResponse;

class NewsDetailController extends Controller
{
    public function __invoke($id, Request $request)
    {
        $auth = $this->authorize('other');
        if (!$auth) {
            return $this->redirect();
        }

        return new NewsDetailResponse;
    }
}
