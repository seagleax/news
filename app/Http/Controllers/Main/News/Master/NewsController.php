<?php

namespace App\Http\Controllers\Main\News\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\Web\News\Master\NewsResponse;

class NewsController extends Controller
{
    public function __invoke(Request $request)
    {
        return new NewsResponse;
    }

    public function index(Request $request)
    {
        $auth = $this->authorize('view');
        if (!$auth) {
            return $this->redirect();
        }

        $data['title'] = 'Manage News';
        return view('page.news.master.view', $data);
    }
}
