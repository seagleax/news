<?php

namespace App\Http\Controllers\Main\News\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Responses\Web\News\Master\NewsSaveResponse;

class NewsCreateController extends Controller
{
    public function add()
    {
        $auth = $this->authorize('add');
        if (!$auth) {
            return $this->redirect();
        }

        $data['title'] = 'Add New News';
        return view('page.news.master.add', $data);
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'news_title' => 'required|unique:news_master,news_title',
            'news_content' => 'required',
            'news_image' => 'required',
        ]);

        if($validator->fails()) {
            return response()->json([
                'code' => 422,
                'message' => $validator->errors()->first(),
            ], 200);
        }

        return new NewsSaveResponse;
    }
}
