<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/news', 'Api\News\NewsController@index');
Route::put('/news/detail', 'Api\News\NewsController@detail');
Route::post('/news/comment', 'Api\News\NewsController@comment');
