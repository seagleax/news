<?php

use Illuminate\Database\Seeder;

class MenuParentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu_parent')->insert([
            [
                'menu_parent_name' => 'Manage Admin',
                'menu_icon_id'  => 1
            ],
            [
                'menu_parent_name' => 'Manage News',
                'menu_icon_id' => 93
            ]
        ]);
    }
}
